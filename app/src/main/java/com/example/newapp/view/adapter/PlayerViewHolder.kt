package com.example.newapp.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.newapp.databinding.ItemPlayerBinding
import com.example.newapp.model.Player

class PlayerViewHolder(view: View): RecyclerView.ViewHolder(view) {

    val binding = ItemPlayerBinding.bind(view)

    fun render(player: Player, position: Int){
        binding.tvPlayerName.text = player.name
        binding.tvTotalScore.text = player.totalPoints.toString()
        binding.positionBtn.text = (position+1).toString()
        binding.tvBestSubject.text = "Best at: " + player.bestScore(player)
    }

}