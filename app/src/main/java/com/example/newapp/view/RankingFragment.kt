package com.example.newapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newapp.R
import com.example.newapp.databinding.FragmentRankingBinding
import com.example.newapp.databinding.FragmentScoreBinding
import com.example.newapp.model.Player
import com.example.newapp.model.Question
import com.example.newapp.view.adapter.PlayerAdapter
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter

class RankingFragment : Fragment() {
    lateinit var binding: FragmentRankingBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRankingBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView(){
        binding.recyclerRanking.layoutManager=LinearLayoutManager(requireActivity())
        binding.recyclerRanking.adapter=PlayerAdapter(readFile())
    }

    fun readFile(): List<Player> {
        val content = mutableListOf<String>()
        val ranking = mutableListOf<Player>()

        val file = InputStreamReader(requireActivity().openFileInput("Ranking"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content.add(line)
            line = br.readLine()
        }

        for (i in content) {
            var temp = i.split(',')
            var player = Player(temp[0],temp[1].toInt(),temp[2].toInt(),temp[3].toInt(),temp[4].toInt(),temp[5].toInt(),temp[6].toInt())
            ranking.add(player)
        }
        return ranking.sortedByDescending { it.totalPoints }
    }
}
