package com.example.newapp.view

import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.newapp.R
import com.example.newapp.databinding.FragmentGameBinding
import com.example.newapp.model.Player
import com.example.newapp.model.Question
import com.example.newapp.viewmodel.MainViewModel
import java.io.OutputStreamWriter

class GameFragment : Fragment(), View.OnClickListener {

    private lateinit var binding: FragmentGameBinding
    val args:GameFragmentArgs by navArgs()
    lateinit var mainViewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGameBinding.inflate(inflater,container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //ViewModelProvider: para conectar UI con ViewModel
        mainViewModel = ViewModelProvider(requireActivity()).get(MainViewModel::class.java)

        //SafeArgs: recibe nombre del jugador con safeArgs
        mainViewModel.currentPlayer.value = args.playerInfo

        binding.pointsBtn.text= mainViewModel.currentPlayer.value!!.totalPoints.toString()

        //Acttualiza la progress bar con cada segundo
        mainViewModel.seconds.observe(viewLifecycleOwner){
            binding.progressBar2.progress = it
            binding.roundsBtn.text = mainViewModel.rounds.value.toString() + "/15"
        }

        mainViewModel.currentQuestion.observe(viewLifecycleOwner) {

            binding.questionTv.text = it.Question
            binding.answerOneBtn.text = it.Answer1
            binding.answerTwoBtn.text = it.Answer2
            binding.answerThreeBtn.text = it.Answer3
            binding.answerFourBtn.text = it.Answer4
            binding.answerOneBtn.setOnClickListener(this)
            binding.answerTwoBtn.setOnClickListener(this)
            binding.answerThreeBtn.setOnClickListener(this)
            binding.answerFourBtn.setOnClickListener(this)

        }
    }

    override fun onClick(p0: View?) {
        val button = p0 as Button
        val text = button.text.toString()

        binding.answerOneBtn.isClickable = false
        binding.answerTwoBtn.isClickable = false
        binding.answerThreeBtn.isClickable = false
        binding.answerFourBtn.isClickable = false

        mainViewModel.timer.cancel()

        if (text == mainViewModel.currentQuestion.value!!.Solution) {
            button.setBackgroundColor(Color.GREEN)
            mainViewModel.incrementScore()
        }
        else button.setBackgroundColor(Color.RED)

        //DELAY para que se vea el color rojo/verde antes de cambiar a normal
        Handler().postDelayed({
            button.setBackgroundColor(resources.getColor(R.color.purple_500))
            mainViewModel.timer.onFinish()
            },500)

        binding.roundsBtn.text = mainViewModel.rounds.value.toString() + "/15"
        binding.pointsBtn.text = mainViewModel.currentPlayer.value!!.totalPoints.toString()

        if(mainViewModel.rounds.value == 15) {
            //WRITE: Añade info del player al fichero "Ranking", que lo haga una sola vez al llegar a la condicion,
            // si lo pones en el ScoreFragment suelto y giras pantalla se escribira dos veces
            writeFile(mainViewModel.currentPlayer.value!!)
            //Si no cambias el valor de las rondas, cuando juegas otra vez seran 16 porque se guarda
            mainViewModel.rounds.value = 0
            //PROBLEMA TIMER: si no se resetea el timer se quedara congelado donde lo dejes en la ultima ronda
            val action = GameFragmentDirections.actionGameFragmentToScoreFragment(mainViewModel.currentPlayer.value!!)
            Navigation.findNavController(binding.root).navigate(action)
        }
    }

    fun writeFile(player: Player){
        val file = OutputStreamWriter(requireActivity().openFileOutput("Ranking", AppCompatActivity.MODE_APPEND))
        var line= player.name+','+player.totalPoints.toString()+','+player.sciencePoints.toString()+','+player.sportsPoints.toString()+','+player.historyPoints.toString()+','+player.geographyPoints.toString()+','+player.entertainmentPoints.toString()
        file.appendLine(line)
        file.flush()
        file.close()
    }
}