package com.example.newapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.newapp.model.Player
import com.example.newapp.databinding.FragmentPlayerNameBinding


class PlayerNameFragment : Fragment() {
    private lateinit var binding: FragmentPlayerNameBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPlayerNameBinding.inflate(inflater,container, false)

        //Play -> Pantalla juego, pasa player como arg
        binding.playButton.setOnClickListener {
            val playerInfo= Player(binding.nameEt.text.toString(),0,0,0,0,0,0)
            val action = PlayerNameFragmentDirections.actionPlayerNameFragmentToGameFragment(playerInfo)
            Navigation.findNavController(binding.root).navigate(action)
        }

        return binding.root
    }

}