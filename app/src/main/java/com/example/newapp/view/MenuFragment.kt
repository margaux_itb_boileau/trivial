package com.example.newapp.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.newapp.R
import com.example.newapp.databinding.FragmentMenuBinding

class MenuFragment : Fragment() {

    private lateinit var binding: FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMenuBinding.inflate(inflater,container, false)

        // Ranking -> Pantalla Ranking
        binding.rankingButton.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_menuFragment_to_rankingFragment)
        }

        //Help -> Dialog con Instrucciones
        binding.helpButton.setOnClickListener {
            // "This" no funciona como context en fragment -> requireActivity()
            val alertDialog: AlertDialog = AlertDialog.Builder(requireActivity()).create()
            alertDialog.setTitle("Instrucciones")
            alertDialog.setMessage("El objetivo del juego es adivinar el mayor numero de respuestas correctas con tal de obtener la puntuación más alta. Hay 5 categorias de preguntas: Geografia, Historia, Entretenimiento, Ciencia y Deportes")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "OK",
                DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() })
            alertDialog.show()
        }

        //Play -> Pantalla Nombre Usuario
        binding.playButton.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_menuFragment_to_playerNameFragment)
        }

        return binding.root
    }

}