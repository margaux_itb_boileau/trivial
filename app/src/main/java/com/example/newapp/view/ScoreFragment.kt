package com.example.newapp.view

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.newapp.databinding.FragmentScoreBinding
import com.example.newapp.model.Player
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter


class ScoreFragment : Fragment() {
    private lateinit var binding: FragmentScoreBinding
    val args:ScoreFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentScoreBinding.inflate(inflater,container, false)
        val player = args.playerInfo

        binding.scoreValueTv.text = player.totalPoints.toString()

        binding.returnBtn.setOnClickListener {
            val action = ScoreFragmentDirections.actionScoreFragmentToMenuFragment()
            Navigation.findNavController(binding.root).navigate(action)
        }

        binding.shareBtn.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_TEXT, "${player.name} scored ${player.totalPoints} points playing trivial")
            intent.type = "text/plain"
            startActivity(intent)
        }

        return binding.root
    }

}