package com.example.newapp.model

class QuestionProvider {
    companion object {
        val questions = arrayOf<Question>(
            Question("History", "Which city was covered in ashes during the eruption of Mount Vesuvius in 79 AD?", "Rome", "Catania", "Pompeii", "Naples", "Pompeii"),
            Question("History", "How many hills were there in the ancient city of Rome?", "Twelve", "Three", "Seven", "Twenty-One", "Seven"),
            Question("History", "Which of these cities wasn’t part of the Assyrian Empire?", "Ashur", "Nimrud", "Nineveh", "Cyrene", "Cyrene"),
            Question("History", "Which of these wasn’t an ancient Phoenician City?", "Tyre", "Sidon", "Beirut", "Persepolis", "Persepolis"),
            Question("History", "\"Who famously said “Veni, vidi, vici?\"", "Winston Churchill", "Charles de Gaulle", "Julius Caesar", "Alexander the Great", "Julius Caesar"),
            Question("History", "Which of these isn’t a wonder of the Ancient World?", "The Pyramids of Giza", "Hanging Gardens of Babylon", "Lighthouse of Alexandria", "Pantheon of Rome", "Pantheon of Rome"),
            Question("History", "Who was the Pharaoh of Egypt during the time of Moses?", "Hatshepsut", "Ramses II", "King Tut", "Amenhotep III", "Ramses II"),
            Question("History", "Who was the Great Carthaginian General who crossed the Alps and greatly troubled the Romans with his Military Prowess?", "Hamilcar", "Hannibal", "Mago", "Scipio", "Hannibal"),
            Question("History", "The Battle of Actium occurred in which year?", "100 BC", "72 BC", "31 BC", "100 AD", "31 BC"),
            Question("History", "Which of these wasn’t one of the Five Good Emperors of Rome?", "Trajan", "Caligula", "Marcus Aurelius", "Hadrian", "Caligula"),
            Question("History", "Vercingetorix was a king and chieftain of which ancient peoples?", "Gauls", "Lusitanis", "Scythians", "Illyrians", "Gauls"),
            Question("Science", "What does the Richter scale measure?", "Wind Speed", "Temperature", "Tornado Strength", "Earthquake intensity", "Earthquake intensity"),
            Question("Science", "Who was the first woman to win a Nobel Prize?", "Mother Teresa", "Marie Curie", "Jane Adams", "Alva Myrdal", "Marie Curie"),
            Question("Science", "How many bones are there in an adult human body?", "186", "206", "286", "306", "206"),
            Question("Science", "What is the highest mountain in Japan?", "Mount Tate", "Mount Kita", "Mount Fuji", "Mount Yari", "Mount Fuji"),
            Question("Science", "Which chemical element has Ag as a symbol?", "Gold", "Silver", "Iron", "Carbon", "Silver"),
            Question("Science", "How many elements are there on the periodic table?", "58", "78", "98", "118", "118"),
            Question("Science", "A Brief History of Time is a biographical documentary about which famous scientist?", "Stephen Hawking", "Alan Turing", "Nikola Tesla", "Louis Pasteur", "Stephen Hawking"),
            Question("Science", "Which one of these blood types is the rarest?", "A positive", "O positive", "B Negative", "AB Negative", "AB Negative"),
            Question("Science", "What does a ‘light year’ measure?", "Sound", "Distance", "Speed", "Time", "Distance"),
            Question("Science", "Photosynthesis is a process created by?", "Plants", "Animals", "Food", "Cameras", "Plants"),
            Question("Science", "In which food will you find the most vitamin C?", "Meat", "Fish", "Orange", "Cereal", "Orange"),
            Question("Science", "Which one of the following is the study of Earth?", "Biology", "Geology", "Chemistry", "Astronomy", "Geology"),
            Question("Science", "What is Earth’s only natural satellite?", "The sun", "The moon", "Mars", "Jupiter", "The moon"),
            Question("Science", "What is a group of atoms called?", "Proton", "Neutron", "Molecule", "Bond", "Molecule"),
            Question("Entertainment", "What is the most viewed movie in cinemas?", "Avengers: Endgame", "Avatar", "Harry Potter", "Star Wars", "Avengers: Endgame"),
            Question("Entertainment", "In the TV show New Girl which actress plays Jessica Day?", "Zooey Deschanel", "Kaley Cuoco", "Jennifer Aniston", "Alyson Hannigan", "Zooey Deschanel"),
            Question("Entertainment", "In the Big Bang Theory what is the name of Sheldon and Leonard’s neighbour?", "Penny", "Patty", "Lily", "Jessie", "Penny"),
            Question("Entertainment", "What is the name of the main character in Pride and Prejudice?", "Bernadette Smith", "Poppy Williams", "Elizabeth Bennet", "Maggie Johnson", "Elizabeth Bennet"),
            Question("Entertainment", "Which band released the song “Wonderwall” in the 90s?", "Oasis", "Joy Division", "The Verge", "Nirvana", "Oasis"),
            Question("Entertainment", "Which author is known for creating Hercule Poirot?", "James Patterson", "Stephen King", "Arthur Conan Doyle", "Agatha Christie", "Agatha Christie"),
            Question("Entertainment", "\"When I find myself in times of trouble Mother Mary comes to me\" is the opening line of which song?", "Smells like teen spirit – Nirvana", "Get lucky – Daft Punk", "Sweet Child O’ Mine – Gun N’ Roses", "Let it be – The Beatles", "Let it be – The Beatles"),
            Question("Entertainment", "What is the name of the dog in Tintin?", "Snowy", "Flakes", "Dottie", "Luna", "Snowy"),
            Question("Entertainment", "Who released the song “Girls Just Want To Have Fun” in the 80s?", "Blondie", "Cyndi Lauper", "A-ha", "Bonnie Tyler", "Cyndi Lauper"),
            Question("Entertainment", "Which actress played Sally Draper in Mad Men?", "January Jones", "Christina Hendricks", "Kiernan Shipka", "Elisabeth Moss", "Kiernan Shipka"),
            Question("Entertainment", "Which Friends character’s famous pickup line is “How you doin’?", "Joey", "Ross", "Chandler", "Mike", "Joey"),
            Question("Entertainment", "What is Marshall’s job in How I met your mother?", "Architect", "Lawyer", "Teacher", "Journalist", "Lawyer"),
            Question("Entertainment", "What’s Garfield favourite food?", "Pizza", "Lasagna", "Burger", "Sandwich", "Lasagna"),
            Question("Entertainment", "Which one of the following artists wasn’t part of the Rolling Stones?", "Mick Jagger", "Keith Richards", "Charlie Watts", "Jimmy Page", "Jimmy Page"),
            Question("Entertainment", "Which one of the following is not a character in the cartoon “The Powerpuff Girls”?", "Blossom", "Butterfly", "Bubbles", "Buttercup", "Butterfly"),
            Question("Sports", "When did Salt Lake City host the Winter Olympics?", "1992", "1998", "2002", "2008", "2002"),
            Question("Sports", "In which city is the Juventus Football Club based?", "Turin", "Barcelona", "Manchester", "Marseille", "Turin"),
            Question("Sports", "Which country is the footballer Cristiano Ronaldo from?", "Spain", "Brazil", "Uruguay", "Portugal", "Portugal"),
            Question("Sports", "Which country won the first Football World Cup in 1930?", "Brazil", "Portugal", "Italy", "Uruguay", "Uruguay"),
            Question("Sports", "In which city were the 2000 Summer Olympics held?", "London", "Paris", "Barcelona", "Sydney", "Sydney"),
            Question("Sports", "How many players are in a cricket team?", "8", "9", "10", "11", "11"),
            Question("Sports", "After how many Year’s FIFA World Cup held?", "2 Years", "3 Years", "4 Years", "Every Year", "4 Years"),
            Question("Sports", "Who won the FIFA World Cup in 2018?", "France", "Germany", "Portugal", "Uraguay", "France"),
            Question("Sports", "Which is the Largest Football Stadium in the World?", "Salt Lake Stadium", "Rungrado 1st of May Stadium", "AT&T Stadium", "Melbourne Cricket Ground", "Rungrado 1st of May Stadium"),
            Question("Sports", "How many players are there in a football (soccer), team?", "7", "9", "11", "13", "11"),
            Question("Sports", "Which sport is not played with a ball?", "Baseball", "Soccer", "Basketball", "Ice hockey", "Ice hockey"),
            Question("Sports", "With which team did Michael Jordan win 6 titles?", "Brooklyn Nets", "Phoenix Suns", "Los Angeles Lakers", "Chicago Bulls", "Chicago Bulls"),
            Question("Sports", "When is the Super Bowl?", "First Sunday in February", "Second Sunday in February", "First Sunday in March", "Second Sunday in February", "First Sunday in February"),
            Question("Sports", "Who was the first gymnast to be awarded a perfect score of 10.0 at the Olympic Games?", "Nadia Comăneci", "Simone Biles", "Gabby Douglas", "Olga Korbut", "Nadia Comăneci"),
            Question("Sports", "What are the National Sports of China?", "Table Tennis", "Baseball", "Cricket", "Swimming", "Table Tennis"),
        )

        fun randomQuestion(): Question {
            val i: Int = (0..questions.size - 1).random()
            return questions[i]
        }
    }
}