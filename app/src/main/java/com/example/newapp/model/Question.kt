package com.example.newapp.model

data class Question(val subject: String, val Question:String, val Answer1:String, val Answer2: String, val Answer3: String, val Answer4: String, val Solution:String)