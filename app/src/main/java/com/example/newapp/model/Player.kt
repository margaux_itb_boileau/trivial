package com.example.newapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Player(val name:String, var totalPoints:Int, var historyPoints:Int, var sciencePoints:Int, var sportsPoints:Int, var entertainmentPoints:Int, var geographyPoints:Int): Parcelable{

    fun bestScore(player:Player): String{
        val listOfPoints = arrayListOf(player.sciencePoints,player.historyPoints, player.sportsPoints,player.entertainmentPoints, player.geographyPoints)
        when (listOfPoints.max()){
            player.sciencePoints -> return "Science"
            player.historyPoints -> return "History"
            player.sportsPoints -> return "Sports"
            player.entertainmentPoints -> return "Entertainment"
            player.geographyPoints -> return "Geography"
        }
        return "None"
    }
}