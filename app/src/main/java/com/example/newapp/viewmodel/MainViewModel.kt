package com.example.newapp.viewmodel

import android.os.CountDownTimer
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.newapp.model.Player
import com.example.newapp.model.Question
import com.example.newapp.model.QuestionProvider

class MainViewModel: ViewModel() {
    var currentQuestion = MutableLiveData<Question>()
    val currentPlayer = MutableLiveData<Player>()
    lateinit var timer : CountDownTimer
    val seconds = MutableLiveData<Int>()
    var rounds = MutableLiveData<Int>()

    //Acciones al iniciar, si pones startTimer() al principio del fragment ser repetirá cuando gires pantalla y tendras dos timers ejecutandoese
    init {
        getRandomQuestion()
        rounds.value = 1
        startTimer()
    }

    fun getRandomQuestion() {
        val newQuestion = QuestionProvider.randomQuestion()
        currentQuestion.postValue(newQuestion)
    }

    fun startTimer() {
        timer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                seconds.value = (millisUntilFinished/1000).toInt()
            }
            override fun onFinish() {
                rounds.value = rounds.value!! + 1
                getRandomQuestion()
                if(rounds.value != 15) {
                    startTimer()
                }
            }
        }.start()
    }

    fun incrementScore() {
        when (currentQuestion.value!!.subject) {
            "History" -> currentPlayer.value!!.historyPoints += 10
            "Science" -> currentPlayer.value!!.sciencePoints += 10
            "Sports" -> currentPlayer.value!!.sportsPoints += 10
            "Entertainment" -> currentPlayer.value!!.entertainmentPoints += 10
            "Geography" -> currentPlayer.value!!.geographyPoints += 10
        }
        currentPlayer.value!!.totalPoints += 10
    }
}